//1640570
//Leon Hazurin
package movies.importer;
import java.util.*;
public class LowercaseProcessor extends Processor{

	public LowercaseProcessor(String srcDir, String outDir) {
		super(srcDir, outDir, true);
	}

	public ArrayList<String> process(ArrayList<String> input){

		ArrayList<String> asLower = new ArrayList<String>();
		input.replaceAll(String::toLowerCase);
		for(String arrayItem : input) {
			asLower.add(arrayItem);
		}
		return asLower;
	}
}
