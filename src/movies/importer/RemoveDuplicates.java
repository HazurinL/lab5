//1640570
//Leon Hazurin
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	public RemoveDuplicates(String srcDir, String outDir) {
		super(srcDir, outDir, false);
	}
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> noDuplicates = new ArrayList<String>();
		for(String element : input) {
			if(!noDuplicates.contains(element)) {
				noDuplicates.add(element);
			}
		}
		return noDuplicates;
	}
}
